#!/usr/bin/php
<?php
class Counters
{
    private $files_scanned = 0;
    private $files_moved = 0;
    private $files_errors = 0;
    private $files_deleted = 0;
    private $directory_scanned = 0;
    private $directory_created = 0;

    private $start;

    public function __construct()
    {
        $this->start = microtime(true);
    }

    public function inc_files_scanned()
    {
        $this->files_scanned++;
    }

    public function inc_files_moved()
    {
        $this->files_moved++;
    }

    public function inc_files_deleted()
    {
        $this->files_deleted++;
    }

    public function inc_files_errors()
    {
        $this->files_errors++;
    }

    public function inc_directory_scanned()
    {
        $this->directory_scanned++;
    }

    public function inc_directory_created()
    {
        $this->directory_created++;
    }

    public function report()
    {
        info('-----------------------------------------');
        info("Démarrée à " . date('H:i:s, d/m/Y', $this->start));
        info("Terminé à " . date('H:i:s, d/m/Y'));
        info();
        info('------');
        info('Fichiers copiés : ' . $this->files_moved);
        info('Fichiers supprimés : ' . $this->files_deleted);
        info('Fichiers en erreur : ' . $this->files_errors);
        info('Fichiers scannés : ' . $this->files_scanned);
        info('Dossiers créés: ' . $this->directory_created);
        info('Dossiers scannés: ' . $this->directory_scanned);
        info('-------');
        info('Enjoy :)');
    }
}

function usage($message = null)
{
    info("Usage : ./meta-sorter.php <source> <destination>");
    info();
    info("\tsource\t\tRépertoire source où se situe les fichiers à trier");
    info("\tdestination\tRépertoire de destination des fichiers");
    exit;
}

function info($line = '')
{
    global $verbosity;
    if ($verbosity >= 0) {
        echo $line . PHP_EOL;
    }
}

function warning($line)
{
    echo $line . PHP_EOL;
}

function debug($line = '')
{
    global $verbosity;
    if ($verbosity >= 1) {
        echo $line . PHP_EOL;
    }
}

function process_files_in_dir($path)
{
    global $counters;
    $counters->inc_directory_scanned();
    debug("Listing des fichiers du dossier : " . $path);
    if ($handle = opendir($path)) {
        while ($entry = readdir($handle)) {
            if (in_array($entry, ['.', '..'])) {
                continue;
            }

            debug("switch de traitement fichier/dossier : " . $path . DIRECTORY_SEPARATOR . $entry);
            if (is_dir($path . DIRECTORY_SEPARATOR . $entry)) {
                process_files_in_dir($path . DIRECTORY_SEPARATOR . $entry);
            } elseif (is_file($path . DIRECTORY_SEPARATOR . $entry)) {
                process_file($path . DIRECTORY_SEPARATOR . $entry);
            }
        }
        closedir($handle);
        clean_up_folders($path);
    }
}

function get_sub_path($filepath)
{
    global $counters;
    debug("Construction du chemin de destination pour le fichier : " . $filepath);
    $filename = basename($filepath);
    if (preg_match('/^[0-9]{13}\./', $filename)) {
        $exploded = explode('.', $filename);
        $time = reset($exploded);
        $year = date('Y', $time / 1000);
        $month = date('m', $time / 1000);
    } elseif (preg_match('/^[0-9]{6}_/', $filename)) {
        $year = "20" . substr($filename, 0, 2);
        $month = substr($filename, 2, 2);
    } elseif (preg_match('/^[0-9]{8}/', $filename)) {
        $year = substr($filename, 0, 4);
        $month = substr($filename, 4, 2);
    } elseif (preg_match('/^(IMG|VID)_[0-9]{8}/', $filename)) {
        $year = substr($filename, 4, 4);
        $month = substr($filename, 8, 2);
    } elseif (preg_match('/^(SAVE)_[0-9]{8}/', $filename)) {
        $year = substr($filename, 5, 4);
        $month = substr($filename, 9, 2);
    } elseif (preg_match('/^Message_[0-9]{13}\./', $filename)) {
        $exploded = explode('.', $filename);
        $time = reset($exploded);
        $year = date('Y', str_replace('Message_', '', $time) / 1000);
        $month = date('m', str_replace('Message_', '', $time) / 1000);
    } else {
        // manage if (preg_match('/^DSC/', $filename) || preg_match('/\.MTS$/', $filename)) {
        // format not manage
        $stat = stat($filepath);
        $year = date('Y', $stat['mtime']);
        $month = date('m', $stat['mtime']);
    }
    if (!isset($month) || !isset($year)) {
        $counters->inc_files_errors();
        warning($filepath . " : impossibilité de déterminer l'année et/ou le mois");
    }

    debug("\t" . $year . DIRECTORY_SEPARATOR . $month);
    return $year . DIRECTORY_SEPARATOR . $month;
}

function create_if_not_exists($path)
{
    global $counters, $dryrun;
    $flag = true;
    debug("vérification présence du dossier : " . $path);
    if (!is_dir($path)) {
        $counters->inc_directory_created();
        debug("création du dossier : " . $path);
        if ($dryrun === false) {
            $flag = mkdir($path, 0755, true);
        } else {
            debug("mkdir($path, 0755, true)");
        }
    }
    return $flag;
}

function copy_files_to($filepath, $to)
{
    global $counters, $dryrun;
    $flag = false;
    $filename = basename($filepath);
    $new_filepath = $to . DIRECTORY_SEPARATOR . $filename;
    debug("tentative de copie : " . $filepath . ' -> ' . $new_filepath);

    if (!file_exists($new_filepath)) {
        debug('fichier inexistant, déplacement à effectuer vers ' . $new_filepath);
        $counters->inc_files_moved();
        if ($dryrun === false) {
            $flag = rename($filepath, $new_filepath);
        } else {
            debug("rename($filepath, $new_filepath);");
        }
    } else {
        debug("fichier existant, comparaison des md5");
        $sorted = exec('md5 -q "' . $new_filepath . '"');
        $original = exec('md5 -q "' . $filepath . '"');
        if ($sorted === $original) {
            debug('fichier identique = suppression');
            $counters->inc_files_deleted();
            if ($dryrun === false) {
                $flag = unlink($filepath);
            } else {
                debug("unlink($filepath);");
            }
        } else {
            $filename = explode('.', $filename);
            $ext = array_pop($filename);///remove extension
            $filename = implode('.', $filename) . '-' . rand(1, 9999) . '.' . $ext; // create new filename
            debug('fichier différent = renommage : ' . $filename);
            if (!file_exists($to . DIRECTORY_SEPARATOR . $filename)) {
                debug('déplacement effectué vers ' . $to . DIRECTORY_SEPARATOR . $filename);
                $counters->inc_files_moved();
                if ($dryrun === false) {
                    $flag = rename($filepath, $to . DIRECTORY_SEPARATOR . $filename);
                } else {
                    debug("rename($filepath, $to . DIRECTORY_SEPARATOR . $filename);");
                }
            }
        }
    }
    return $flag;
}

function process_file($filepath)
{
    global $to_dir, $counters;
    $counters->inc_files_scanned();
    $subpath = get_sub_path($filepath);
    create_if_not_exists($to_dir . DIRECTORY_SEPARATOR . $subpath);
    copy_files_to($filepath, $to_dir . DIRECTORY_SEPARATOR . $subpath);
}

function clean_up_folders($path)
{
    global $dryrun;
    if ($dryrun === false) {
        exec('find "' . $path . '" -type f -name .DS_Store -delete');
        exec('find "' . $path . '" -type d -empty -delete');
    } else {
        info("exec('find \"$path\" -type f -name .DS_Store -delete');");
        info("exec('find \"$path\" -type d -empty -delete'););");
    }
}

$options = getopt("hv", [
    "from:",
    "to:",
    "dry-run"
]);

$verbosity = 0;
$dryrun = false;
if (isset($options['h']) && !empty($options['h'])) {
    usage();
}
if (isset($options['h']) && !empty($options['h'])) {
    usage();
}

if (isset($options['dry-run'])) {
    $dryrun = true;
}
if (!isset($options['from']) || empty($options['from'])) {
    usage();
}
if (!isset($options['from']) || empty($options['from'])
    || empty($options['to']) || empty($options['to'])) {
    usage();
}

$dir = realpath($options['from']);
$to_dir = realpath($options['to']);

if (!is_dir($dir)) {
    usage("Répertoire invalide : " . $dir);
}
if (!is_dir($to_dir)) {
    usage("Répertoire invalide : " . $to_dir);
}

$counters = new Counters();
process_files_in_dir($dir);
$counters->report();